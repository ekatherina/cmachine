//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Katherina🌹 on 4/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var coffeeLabel: UILabel!
    @IBOutlet weak var milkLabel: UILabel!
    @IBOutlet weak var waterLabel: UILabel!
    
    let coffeeMachine = CoffeeMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearLabel()
        let backgroundImageView = UIImageView(image: UIImage(named: "coffee"))
        backgroundImageView.frame = view.frame
        backgroundImageView.contentMode = .scaleAspectFill
        view.addSubview(backgroundImageView)
        view.sendSubviewToBack(backgroundImageView)
    }
    
    func clearLabel() {
        waterLabel.text = String(coffeeMachine.waterInMachine)
        coffeeLabel.text = String(coffeeMachine.coffeeInMachine)
        milkLabel.text = String(coffeeMachine.milkInMachine)
    }
    @IBAction func milkButton(_ sender: UIButton) {
      coffeeMachine.addProduct(choice: 2)
        clearLabel()
    }
    @IBAction func beansButton(_ sender: UIButton) {
        coffeeMachine.addProduct(choice: 0)
        clearLabel()
    }
    @IBAction func waterButton(_ sender: UIButton) {
        coffeeMachine.addProduct(choice: 1)
        clearLabel()
    }
    @IBAction func latteButton(_ sender: UIButton) {
        resultLabel.text = coffeeMachine.makeCoffeeDrink(tag: "Latte", water: 0, coffeeBeans: 200, milk: 300)
        clearLabel()
    }
    @IBAction func cappucinoButton(_ sender: UIButton) {
        resultLabel.text = coffeeMachine.makeCoffeeDrink(tag: "Cappucino", water: 0, coffeeBeans: 150, milk: 200)
        clearLabel()
    }
    
    @IBAction func americanoButton(_ sender: UIButton) {
        resultLabel.text = coffeeMachine.makeCoffeeDrink(tag: "Americano", water: 250, coffeeBeans: 150, milk: 0)
    }
    @IBAction func espressoButton(_ sender: UIButton) {
        resultLabel.text = coffeeMachine.makeCoffeeDrink(tag: "Espresso", water: 150, coffeeBeans: 150, milk: 0)
        clearLabel()
    }
    
    
    
}

