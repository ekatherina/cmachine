//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Katherina🌹 on 4/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class Recipes: NSObject {
    var drink = ""
    var waterNeeded = 0
    var coffeeNeeded = 0
    var milkNeeded = 0
    
    init(drink: String, waterNeeded: Int, coffeeNeeded: Int, milkNeeded: Int){
        self.drink = drink
        self.waterNeeded = waterNeeded
        self.coffeeNeeded = coffeeNeeded
        self.milkNeeded = milkNeeded
    }
}
class CoffeeMachine: NSObject {
    var coffeeInMachine = 200
    var waterInMachine = 100
    var milkInMachine = 100
    var result = ""
    func addProduct(choice: Int) {
        switch choice {
        case 0:
            coffeeInMachine += 100
        case 1:
            waterInMachine += 100
        case 2:
            milkInMachine += 100
        default:
            print ("Errors")
        }
    }
    func makeCoffeeDrink(tag: String, water: Int, coffeeBeans: Int, milk: Int) -> String{
        var errorsCoffeeMake = ""
        
        //Проверка наличия ингридиентов
        if coffeeInMachine < coffeeBeans {
            errorsCoffeeMake = " beans"
            result = "Add:\(errorsCoffeeMake)"
            return result
        } else if waterInMachine < water {
            errorsCoffeeMake = "water"
            result = "Add:\(errorsCoffeeMake)"
            return result
        } else if milkInMachine < milk {
            errorsCoffeeMake = "milk"
            result = "Add:\(errorsCoffeeMake)"
            return result
        }
        else {
            coffeeInMachine = coffeeInMachine - coffeeBeans
            waterInMachine = waterInMachine - water
            milkInMachine = milkInMachine - milk
            result = " \(tag) done"
            return result
        }
        
    }
    
}
